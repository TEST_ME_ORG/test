if __name__ == '__main__':
    exit(0)

from ftplib import FTP


class SimpleFTP(FTP):
    def chkdir(self, dir):
        if self.directory_exists(dir) is False:
            self.mkd(dir)

    def directory_exists(self, dir):
        filelist = []
        self.retrlines('LIST', filelist.append)
        for f in filelist:
            if f.split()[-1] == dir and f.upper().startswith('D'):
                return True
        return False

    def upload(self, s_path, s_name, d_path, d_name):
        try:
            self.chkdir(d_path)
            with open(s_path + s_name, 'rb') as fobj:
                self.storbinary('STOR ' + d_path + "/" + d_name, fobj, 1024)
                print("File: " + d_name + " copy")
        except FileNotFoundError:
            print("FileNotFoundError exception")
