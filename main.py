import json
from SimpleFTP import SimpleFTP as FTP

try:
    with open('json.settings') as data_file:
        SETTINGS = json.load(data_file)
except FileNotFoundError:
    print("FileNotFound exception")
    exit(0)
except json.decoder.JSONDecodeError:
    print("JSONDecode exception")
    exit(0)

try:
    SERVER = SETTINGS['server']['address']
    PORT = SETTINGS['server']['port']
    LOGIN = SETTINGS['server']['login']
    PASSWORD = SETTINGS['server']['password']
except KeyError:
    print("Key exception")
    exit(0)

ftp = FTP()

try:
    ftp.connect(SERVER, PORT)
    ftp.login(LOGIN, PASSWORD)
except TimeoutError:
    print("Timeout exception")
    exit(0)
except ConnectionRefusedError:
    print("ConnectionRefused exception")
    exit(0)

for file in SETTINGS["files"]:
    ftp.upload(SETTINGS["files"][file]["s_path"],
               SETTINGS["files"][file]["s_name"],
               SETTINGS["files"][file]["d_path"],
               SETTINGS["files"][file]["d_name"])

ftp.quit()
